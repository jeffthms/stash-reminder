package com.jeffthms.stash.reminder;

public interface PullRequestReminder {
	static final String REMINDER_START_TIME = "com.jeffthms.stash.reminder.start.time";
	static final String REMINDER_INTERVAL = "com.jeffthms.stash.reminder.interval";
	static final String REMINDER_WINDOW = "com.jeffthms.stash.reminder.window";
	static final String REMINDER_SEND_ON_WEEKEND = "com.jeffthms.stash.reminder.weekends";
	static final String REMINDER_SEND_HTML = "com.jeffthms.stash.reminder.html";
	static final String REMINDER_ALL_PROJECTS = "com.jeffthms.stash.reminder.all.projects";
	static final String DEFAULT_START_TIME = "08:00";
	static final String DEFAULT_INTERVAL = "86400000";	//24 hours
	static final String DEFAULT_WINDOW = "82800000"; 	//23 hours
	static final String DEFAULT_SEND_ON_WEEKEND = "false";
	static final String DEFAULT_SEND_HTML = "true";
	static final String DEFAULT_ALL_PROJECTS = "true";
	
    public void schedule();
    public void reschedule();
}
