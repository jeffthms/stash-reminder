package com.jeffthms.stash.reminder.model;

import net.java.ao.Entity;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

@Table("ProjectEmail")
public interface AoProjectEmail extends Entity {
    Integer getProject();

    void setProject(Integer project);
    
    boolean isEnabled();
    
    void setEnabled(boolean enabled);
}
