package com.jeffthms.stash.reminder.model;

import net.java.ao.Entity;
import net.java.ao.schema.Table;

@Table("UserSetting")
public interface AoUserSetting extends Entity {
	Integer getPerson();

    void setPerson(Integer person);
    
    boolean isSendEmail();
    
    void setSendEmail(boolean sendEmail);
}
