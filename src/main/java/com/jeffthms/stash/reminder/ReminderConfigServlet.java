package com.jeffthms.stash.reminder;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.project.ProjectService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.google.common.collect.Maps;
import com.jeffthms.stash.reminder.model.AoProjectEmail;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.*;
import static com.jeffthms.stash.reminder.PullRequestReminder.*;

public class ReminderConfigServlet extends HttpServlet{
    private static final String START_TIME = "reminderStartTime";
    private static final String INTERVAL = "reminderInterval";
    private static final String WINDOW = "reminderWindow";
    private static final String WEEKEND = "reminderWeekend";
    private static final String HTML = "reminderHtml";
    private static final String PROJECTS = "projects";
    private static final String ALL_PROJECTS = "reminderAllProjects";
    private static final String SELECTED_PROJECTS = "reminderSelectedProjects";
    private static final String ENABLED_PROJECTS = "reminderEnabledProjects";
    
    private String startTimeString;
    private String intervalString;
    private String windowString;
    private String weekendString;
    private String htmlString;
    private String allProjectsString;
    private List<Project> projects;
    
    private static final Logger log = LoggerFactory.getLogger(ReminderConfigServlet.class);
    
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final PermissionValidationService permissionValidationService;
    private final NavBuilder navBuilder;
    private final PullRequestReminder pullRequestReminder;
    private final ProjectService projectService;
    private final ProjectEmailService projectEmailService;
    
    public ReminderConfigServlet(SoyTemplateRenderer soyTemplateRenderer, PermissionValidationService permissionValidationService,
            PluginSettingsFactory pluginSettingsFactory, NavBuilder navBuilder, PullRequestReminder pullRequestReminder,
            ProjectService projectService, ProjectEmailService projectEmailService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.permissionValidationService = permissionValidationService;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.navBuilder = navBuilder;
        this.pullRequestReminder = pullRequestReminder;
        this.projectService = projectService;
        this.projectEmailService = checkNotNull(projectEmailService);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        permissionValidationService.validateForGlobal(Permission.ADMIN);
        PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        
        startTimeString = (String) globalSettings.get(REMINDER_START_TIME);
        intervalString = (String) globalSettings.get(REMINDER_INTERVAL);
        windowString = (String) globalSettings.get(REMINDER_WINDOW);
        weekendString = (String) globalSettings.get(REMINDER_SEND_ON_WEEKEND);
        htmlString = (String) globalSettings.get(REMINDER_SEND_HTML);
        allProjectsString = (String) globalSettings.get(REMINDER_ALL_PROJECTS);
        projects = getAllProjects();
        
        if (StringUtils.isBlank(startTimeString)) {
            startTimeString = PullRequestReminder.DEFAULT_START_TIME;
        }
        if (StringUtils.isBlank(intervalString)) {
            intervalString = PullRequestReminder.DEFAULT_INTERVAL;
        }
        if (StringUtils.isBlank(windowString)) {
            windowString = PullRequestReminder.DEFAULT_WINDOW;
        }
        if (StringUtils.isBlank(htmlString)) {
            htmlString = PullRequestReminder.DEFAULT_SEND_HTML;
        }
        if (StringUtils.isBlank(allProjectsString)) {
            allProjectsString = PullRequestReminder.DEFAULT_ALL_PROJECTS;
        }
        
        List<AoProjectEmail> enabledProjects = projectEmailService.allEnabled();
        HashMap<String, HashMap<String, String>> projectMap = new HashMap<String, HashMap<String, String>>();
        for (Project project : projects) {
        	HashMap<String, String> map = new HashMap<String, String>();
        	map.put("id", "" + project.getId());
        	map.put("key", project.getKey());
        	map.put("name", project.getName());
        	map.put("enabled", "false");
        	projectMap.put(Integer.toString(project.getId()), map);
        }
        for (AoProjectEmail project : enabledProjects) {
        	Project p = projectService.getById(project.getProject());
        	HashMap<String, String> map = new HashMap<String, String>();
        	map.put("id", "" + p.getId());
        	map.put("key", p.getKey());
        	map.put("name", p.getName());
        	map.put("enabled", "true");
        	projectMap.put(Integer.toString(p.getId()), map);
        }
        
        Map<String, Object> context = Maps.newHashMap();
        context.put(START_TIME, startTimeString);
        context.put(INTERVAL, intervalString);
        context.put(WINDOW, windowString);
        context.put(WEEKEND, weekendString);
        context.put(HTML, htmlString);
        context.put(ALL_PROJECTS, allProjectsString);
        context.put(PROJECTS, projectMap);
        
        resp.setContentType("text/html");
        render(resp.getWriter(), "reminder.config", context);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        permissionValidationService.validateForGlobal(Permission.ADMIN);
        PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        
        String startTime = req.getParameter(START_TIME);
        String interval = req.getParameter(INTERVAL);
        String window = req.getParameter(WINDOW);
        String weekend = req.getParameter(WEEKEND);
        String html = req.getParameter(HTML);
        String allProjects = req.getParameter(ALL_PROJECTS);
        String[] selectedProjects = req.getParameterValues(SELECTED_PROJECTS);
        boolean hasErrors = false;
        
        projects = getAllProjects();
        
        if (selectedProjects == null) {
        	selectedProjects = new String[0];
        }
        for (int i=0; i<selectedProjects.length; i++) {
        	projectEmailService.createOrUpdate(Integer.parseInt(selectedProjects[i]), true);
        }
        for (Project project : projects) {
        	List<String> selectedProjectList = Arrays.asList(selectedProjects);
        	if (!selectedProjectList.contains(project.getId().toString())) {
        		projectEmailService.createOrUpdate(project.getId(), false);
        	}
        }
        
        List<AoProjectEmail> enabledProjects = projectEmailService.allEnabled();
        HashMap<String, HashMap<String, String>> projectMap = new HashMap<String, HashMap<String, String>>();
        for (Project project : projects) {
        	HashMap<String, String> map = new HashMap<String, String>();
        	map.put("id", "" + project.getId());
        	map.put("key", project.getKey());
        	map.put("name", project.getName());
        	map.put("enabled", "false");
        	projectMap.put(Integer.toString(project.getId()), map);
        }
        for (AoProjectEmail project : enabledProjects) {
        	Project p = projectService.getById(project.getProject());
        	HashMap<String, String> map = new HashMap<String, String>();
        	map.put("id", "" + p.getId());
        	map.put("key", p.getKey());
        	map.put("name", p.getName());
        	map.put("enabled", "true");
        	projectMap.put(Integer.toString(p.getId()), map);
        }
        
        Map<String, Object> errors = Maps.newHashMap();
        Map<String, Object> context = Maps.newHashMap();
        context.put(START_TIME, startTime);
        context.put(INTERVAL, interval);
        context.put(WINDOW, window);
        context.put(WEEKEND, weekend);
        context.put(HTML, html);
        context.put(ALL_PROJECTS, allProjects);
        context.put(PROJECTS, projectMap);
        
        if (StringUtils.isNotBlank(startTime)) {
            try {
                new SimpleDateFormat("HH:mm").parse(startTime);
            }
            catch (ParseException e) {
                hasErrors = true;
                errors.put(START_TIME, "Invalid format, please use 'HH:mm'.");
            }
        }
        if (StringUtils.isNotBlank(interval)) {
            long intervalLong = 0;
            try {
                 intervalLong = Long.parseLong(interval);
                 if (intervalLong <= 0) {
                    hasErrors = true;
                    errors.put(INTERVAL, "Please enter a number greater than 0.");
                }
            }
            catch (NumberFormatException e) {
                hasErrors = true;
                errors.put(INTERVAL, "Not a number, please use a number.");
            }
        }
        if (StringUtils.isNotBlank(window)) {
            long windowLong = 0;
            try {
                 windowLong = Long.parseLong(window);
                 if (windowLong <= 0) {
                    hasErrors = true;
                    errors.put(WINDOW, "Please enter a number greater than 0.");
                }
            }
            catch (NumberFormatException e) {
                hasErrors = true;
                errors.put(WINDOW, "Not a number, please use a number.");
            }
        }
        if (StringUtils.isNotBlank(weekend)) {
            try {
                Boolean.parseBoolean(weekend);
            } catch (Exception e) {
                hasErrors = true;
                errors.put(WEEKEND, "Unable to parse boolean: " + e);
            }
        } else {
            weekend = "false";
        }
        if (StringUtils.isNotBlank(html)) {
            try {
                Boolean.parseBoolean(html);
            } catch (Exception e) {
                hasErrors = true;
                errors.put(HTML, "Unable to parse boolean: " + e);
            }
        } else {
            html = "false";
        }
        if (StringUtils.isNotBlank(allProjects)) {
            try {
                Boolean.parseBoolean(allProjects);
            } catch (Exception e) {
                hasErrors = true;
                errors.put(ALL_PROJECTS, "Unable to parse boolean: " + e);
            }
        } else {
            allProjects = "false";
        }
        
        if (hasErrors) {
            context.put("errors", errors);
            resp.setContentType("text/html");
            render(resp.getWriter(), "reminder.config", context);
            return;
        }
        
        globalSettings.put(REMINDER_START_TIME, startTime);
        globalSettings.put(REMINDER_INTERVAL, interval);
        globalSettings.put(REMINDER_WINDOW, window);
        globalSettings.put(REMINDER_SEND_ON_WEEKEND, weekend);
        globalSettings.put(REMINDER_SEND_HTML, html);
        globalSettings.put(REMINDER_ALL_PROJECTS, allProjects);
        
        pullRequestReminder.reschedule();
        
        context.put("saved", "true");

        resp.setContentType("text/html");
        render(resp.getWriter(), "reminder.config", context);
    }
    
    private void render(Appendable appendable, String template, Map<String, Object> context) throws IOException {
        try {
            soyTemplateRenderer.render(appendable, "com.jeffthms.stash.reminder:reminder-soy-templates",
                    template, context);
        } catch (SoyException e) {
            appendable.append("Failed to render " + template + ": " + e.getMessage());
            log.error("Failed to render " + template, e);
        }
    }
    
    private List<Project> getAllProjects() {
    	PageRequest pageRequest = new PageRequestImpl(0, 5);
    	boolean lastPage = false;
    	List<Project> projectList = new ArrayList<Project>();
    	
    	while (pageRequest != null && !lastPage) {
    		Page<? extends Project> projects = projectService.findAll(pageRequest);
        	if (projects.getIsLastPage()) {
        		lastPage = true;
        	}
        	
    		for (Project project : projects.getValues()) {
    			projectList.add(project);
                log.info("Adding " + project + "to list of all projects");
            }
        	
        	pageRequest = projects.getNextPageRequest();
        }
    	
    	return projectList;
    }
}