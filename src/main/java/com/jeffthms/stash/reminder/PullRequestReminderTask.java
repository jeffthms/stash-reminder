package com.jeffthms.stash.reminder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.avatar.AvatarRequest;
import com.atlassian.stash.avatar.AvatarService;
import com.atlassian.stash.mail.MailMessage;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestOrder;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestSearchRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestState;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Operation;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.jeffthms.stash.reminder.model.AoProjectEmail;
import com.jeffthms.stash.reminder.model.AoUserSetting;

public class PullRequestReminderTask implements PluginJob {
	private final Logger logger = Logger.getLogger(PullRequestReminderTask.class);
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM - HH:mm a");
	
	private MailService mailService = null;
	private SecurityService securityService = null;
	private PullRequestService pullRequestService = null;
	private ApplicationPropertiesService applicationPropertiesService = null;
	private NavBuilder navBuilder = null;
	private PluginSettingsFactory pluginSettingsFactory = null;
	private AvatarService avatarService = null;
	private SoyTemplateRenderer soyTemplateRenderer = null;
	private ProjectEmailService projectEmailService = null;
	private UserSettingService userSettingService = null;
	 
    /**
     * Executes this job.
     *
     * @param jobDataMap any data the job needs to execute. Changes to this data will be remembered between executions.
     */
    public void execute(Map<String, Object> jobDataMap) {
        final PullRequestReminderImpl reminder = (PullRequestReminderImpl)jobDataMap.get(PullRequestReminderImpl.KEY);
        pullRequestService = (PullRequestService) jobDataMap.get("pullRequestService");
        securityService = (SecurityService) jobDataMap.get("securityService");
        mailService = (MailService) jobDataMap.get("mailService");
        applicationPropertiesService = (ApplicationPropertiesService) jobDataMap.get("applicationPropertiesService");
        navBuilder = (NavBuilder) jobDataMap.get("navBuilder");
        pluginSettingsFactory = (PluginSettingsFactory) jobDataMap.get("pluginSettingsFactory");
        avatarService = (AvatarService) jobDataMap.get("avatarService");
        soyTemplateRenderer = (SoyTemplateRenderer) jobDataMap.get("soyTemplateRenderer");
        projectEmailService = (ProjectEmailService) jobDataMap.get("projectEmailService");
        userSettingService = (UserSettingService) jobDataMap.get("userSettingService");
        
        assert reminder != null || pullRequestService != null || securityService != null || mailService != null || applicationPropertiesService != null || navBuilder != null || pluginSettingsFactory != null || avatarService != null || soyTemplateRenderer != null || projectEmailService != null || userSettingService != null;
        
        final PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        
        logger.info("Finding all open pull requests");
        
        long reminderWindow = getReminderWindow(globalSettings);
        reminder.setLastRun(new Date());
        PageRequest pageRequest = new PageRequestImpl(0, 10);
        boolean lastPage = false;
        HashMap<StashUser, List<PullRequest>> emailMap = new HashMap<StashUser, List<PullRequest>>();
        
        if (!mailService.isHostConfigured()) {
        	logger.error("Mail host is not configured!");
        	return;
        }
        if (isDoNotSendOnWeekend(globalSettings)) {
        	logger.info("Settings prevent running today!");
        	return;
        }
        
        while (pageRequest != null && !lastPage) {
        	final Page<PullRequest> pullRequestsPage = getOpenPullRequests(securityService, pullRequestService, pageRequest);
        	if (pullRequestsPage.getIsLastPage()) {
        		lastPage = true;
        	}
        	
        	for (PullRequest pullRequest : pullRequestsPage.getValues()) {
        		if (!isExpired(pullRequest.getCreatedDate(), new Date(), reminderWindow)) {
        			logger.info("Pull request is NOT older than " + reminderWindow / 1000 + " seconds, no emails will be sent.");
        		} else if (!isSendEmailToProject(pullRequest, globalSettings, projectEmailService)) {
        			logger.info("Project is not configured to receive emails for this pull request, no emails will be sent.");
        		} else {
        			logger.info(pullRequest.getTitle() + " created on " + pullRequest.getCreatedDate());
        			
	        		Set<PullRequestParticipant> reviewers = pullRequest.getReviewers();
	        		
	        		for (PullRequestParticipant participant : reviewers) {
	        			if (!participant.isApproved()) {
	        				StashUser user = participant.getUser();
	        				if (isSendEmailToUser(user, userSettingService)) {
		        				logger.info(user.getDisplayName() + " (" + user.getEmailAddress() + ") has not approved this pull request");
		        				
		        				if (emailMap.containsKey(user)) {
		        					List<PullRequest> tempList = emailMap.get(user);
		        					tempList.add(pullRequest);
		        					emailMap.put(user, tempList);
		        				} else {
		        					List<PullRequest> tempList = new ArrayList<PullRequest>();
		        					tempList.add(pullRequest);
		        					emailMap.put(user, tempList);
		        				}
	        				} else {
		        				logger.info(user.getDisplayName() + " (" + user.getEmailAddress() + ") has not approved this pull request but has turned off notifications, no emails will be sent");
	        				}
	        			}
	        		}
        		}
            }
        	
        	pageRequest = pullRequestsPage.getNextPageRequest();
        }
        
        sendReminderEmails(emailMap, isHtmlEmail(globalSettings));
    }
    
    private Page<PullRequest> getOpenPullRequests(SecurityService securityService, final PullRequestService pullRequestService,
    		final PageRequest pageRequest) {
    	return (Page<PullRequest>) securityService.doWithPermission("Finding open pull requests", Permission.ADMIN, new Operation<Object, RuntimeException>() {
            @Override
            public Object perform() {
            	return pullRequestService.search(new PullRequestSearchRequest.Builder()
			        .order(PullRequestOrder.OLDEST)
			        .state(PullRequestState.OPEN)
			        .build(), pageRequest);            			            			
            }
        });
    }
    
    private boolean isExpired(Date a, Date b, long max) {
    	long difference = b.getTime() - a.getTime();
    	
    	if (Math.abs(difference) > max) {
    		return true;
    	}
    	
    	return false;
    }
    
    private void sendReminderEmails(HashMap<StashUser, List<PullRequest>> emailMap, boolean html) {
    	String contentType = "";
    	String body = "";
    	for (Map.Entry<StashUser, List<PullRequest>> entry : emailMap.entrySet()) {
    		Map<String, Object> emailData = new HashMap<String, Object>();
    		emailData.put("pullRequests", constructPullRequestList(entry.getValue()));
    		
    		if (html) {
    			contentType = "text/html; charset=UTF-8";
        		
        		try {
    				body = soyTemplateRenderer.render("com.jeffthms.stash.reminder:reminder-soy-templates", "reminder.email", emailData);
    			} catch (SoyException e) {
    				logger.error("Unable to generate email body - " + e.getMessage());
    				return;
    			}
    		} else {
    			contentType = "text/plain; charset=UTF-8";
    			
    			body = constructTextEmailBody(emailData);
    		}
    	
    		String subject = constructEmailSubject(entry.getValue().size());
    		String to = entry.getKey().getEmailAddress();
    		
    		mailService.submit(new MailMessage.Builder()
		        .to(to)
		        .subject(subject)
		        .text(body)
		        .header("Content-Type", contentType)
		        .build());
    	}
    }
    
    private String constructTextEmailBody(Map<String, Object> emailData) {
    	StringBuilder sb = new StringBuilder();
    	
    	List<Map<String, String>> pullRequests = (List<Map<String, String>>) emailData.get("pullRequests");
    	int index = 1;
    	
    	for (Map<String, String> pullRequest : pullRequests) {
    		sb.append(String.format("%d. %s/%s - %s (%s)\n", index, pullRequest.get("project"), pullRequest.get("repository"), pullRequest.get("title"), pullRequest.get("url")));
    		index++;
    	}
    	
    	return sb.toString();
    }
    
    private String constructEmailSubject(int pullRequestCount) {
    	String request = "request";
    	request = pullRequestCount == 1 ? request : request  + "s";
    	
    	return String.format("%d pull %s waiting your approval", pullRequestCount, request);
    }
    
    private Map<String, String> constructPullRequestData(PullRequest pullRequest) {
    	Map<String, String> data = new HashMap<String, String>();
    	Repository repository = pullRequest.getToRef().getRepository();
    	
    	StashUser author = pullRequest.getAuthor().getUser();
    	
    	data.put("project", repository.getProject().getName());
    	data.put("repository", repository.getName());
    	data.put("title", pullRequest.getTitle());
    	data.put("url", constructPullRequestUrl(pullRequest, repository));
    	data.put("created", dateFormat.format(pullRequest.getCreatedDate()));
    	data.put("author", author.getDisplayName());
    	data.put("authorAvatar", avatarService.getUrlForPerson(author, new AvatarRequest(false, 16)));
    	
    	return data;
    }
    
    private List<Map<String, String>> constructPullRequestList(List<PullRequest> pullRequests) {
    	List<Map<String, String>> results = new ArrayList<Map<String, String>>();
    	
    	for (PullRequest pullRequest : pullRequests) {
    		results.add(constructPullRequestData(pullRequest));
    	}
    	
    	return results;
    }

    private String constructPullRequestUrl(PullRequest pullRequest, Repository repository) {
    	return navBuilder.repo(repository).pullRequest(pullRequest.getId()).buildAbsolute();
    }
    
    private long getReminderWindow(PluginSettings pluginSettings) {
    	String windowString = (String) pluginSettings.get(PullRequestReminder.REMINDER_WINDOW);
		long window;
		
		if (windowString == null) {
			windowString = PullRequestReminder.DEFAULT_WINDOW;
		}
		
		try {
			window = Long.parseLong(windowString);
		} catch (NumberFormatException e) {
			throw new IllegalStateException("Interval is not a number", e);
		}
				
		return window;
    }
    
    private boolean isDoNotSendOnWeekend(PluginSettings pluginSettings) {
    	String weekendsString = (String) pluginSettings.get(PullRequestReminder.REMINDER_SEND_ON_WEEKEND);
    	boolean weekends;
    	
    	if (weekendsString == null) {
    		weekendsString = PullRequestReminder.DEFAULT_SEND_ON_WEEKEND;
    	}
    	
    	try {
    		weekends = Boolean.parseBoolean(weekendsString);
    	} catch (Exception e) {
    		throw new IllegalStateException("Weekends setting is not a boolean", e);
    	}
    	
    	Calendar calendar = new GregorianCalendar();
    	int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    	
    	if ((Calendar.SATURDAY == dayOfWeek || Calendar.SUNDAY == dayOfWeek) && !weekends) {
    		return true;
    	}
    	
    	return false;
    }
    
    private boolean isHtmlEmail(PluginSettings pluginSettings) {
    	String htmlString = (String) pluginSettings.get(PullRequestReminder.REMINDER_SEND_HTML);
		boolean html;
    	    	
    	if (htmlString == null) {
    		htmlString = PullRequestReminder.DEFAULT_SEND_HTML;
    	}
    	
    	try {
    		html = Boolean.parseBoolean(htmlString);
    	} catch (Exception e) {
    		throw new IllegalStateException("Weekends setting is not a boolean", e);
    	}
    	    	
    	return html;
    }
    
    private boolean isEmailAllProjects(PluginSettings pluginSettings) {
    	String allProjectsString = (String) pluginSettings.get(PullRequestReminder.REMINDER_ALL_PROJECTS);
		boolean allProjects;
    	    	
    	if (allProjectsString == null) {
    		allProjectsString = PullRequestReminder.DEFAULT_ALL_PROJECTS;
    	}
    	
    	try {
    		allProjects = Boolean.parseBoolean(allProjectsString);
    	} catch (Exception e) {
    		throw new IllegalStateException("All projects setting is not boolean", e);
    	}
    	    	
    	return allProjects;
    }
    
    private boolean isSendEmailToProject(PullRequest pullRequest, PluginSettings pluginSettings, ProjectEmailService projectEmailService) {
    	if (isEmailAllProjects(pluginSettings)) {
    		return true;
    	} else {
    		List<AoProjectEmail> enabledProjects = projectEmailService.allEnabled();
    		List<Integer> enabledProjectIds = new ArrayList<Integer>();
    		for (AoProjectEmail project : enabledProjects) {
    			enabledProjectIds.add(project.getProject());
    		}
    		if (enabledProjectIds.contains(pullRequest.getToRef().getRepository().getProject().getId())) {
    			return true;
    		}
    	}
    	return false;
    }
    
    private boolean isSendEmailToUser(StashUser user, UserSettingService userSettingService) {
    	AoUserSetting userSetting = userSettingService.get(user.getId());
    	
    	if (userSetting == null) {
    		return true;
    	} else {
    		return userSetting.isSendEmail();
    	}
    }
}
