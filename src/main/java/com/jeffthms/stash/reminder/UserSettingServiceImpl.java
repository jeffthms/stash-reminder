package com.jeffthms.stash.reminder;

import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.jeffthms.stash.reminder.model.AoProjectEmail;
import com.jeffthms.stash.reminder.model.AoUserSetting;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

public class UserSettingServiceImpl implements UserSettingService {
	private final ActiveObjects ao;
	
	public UserSettingServiceImpl(ActiveObjects ao) {
		this.ao = checkNotNull(ao);
	}

	@Override
	public AoUserSetting add(int user) {
		final AoUserSetting userSetting = ao.create(AoUserSetting.class);
		userSetting.setPerson(user);
		userSetting.setSendEmail(true);
		userSetting.save();
		return userSetting;
	}

	@Override
	public AoUserSetting get(int user) {
		List<AoUserSetting> userSettings = newArrayList(ao.find(AoUserSetting.class, "PERSON = ?", user));
		if (userSettings.size() > 0) {
			return userSettings.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<AoUserSetting> all() {
		return newArrayList(ao.find(AoUserSetting.class));
	}

	@Override
	public AoUserSetting setSendEmail(int user, boolean sendEmail) {
		AoUserSetting userSetting = get(user);
		if (userSetting == null) {
			return null;
		}
		userSetting.setSendEmail(sendEmail);
		userSetting.save();
		return userSetting;
	}

}
