package com.jeffthms.stash.reminder;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.jeffthms.stash.reminder.model.AoProjectEmail;

public class ProjectEmailServiceImpl implements ProjectEmailService {
	
	private final ActiveObjects ao;
	private static final Logger LOG = LoggerFactory.getLogger(ProjectEmailServiceImpl.class);
	
	public ProjectEmailServiceImpl(ActiveObjects ao) {
		this.ao = checkNotNull(ao);
	}

	@Override
	public AoProjectEmail add(int project, boolean enabled) {
		final AoProjectEmail projectEmail = ao.create(AoProjectEmail.class);
		projectEmail.setProject(project);
		projectEmail.setEnabled(enabled);
		projectEmail.save();
        return projectEmail;
	}
	
	@Override
	public AoProjectEmail add(int project) {
		return add(project, true);
	}

	@Override
	public List<AoProjectEmail> all() {
		return newArrayList(ao.find(AoProjectEmail.class));
	}

	@Override
	public AoProjectEmail get(int project) {
		List<AoProjectEmail> projects = newArrayList(ao.find(AoProjectEmail.class, "PROJECT = ?", project));
		if (projects.size() > 0) {
			return projects.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<AoProjectEmail> allEnabled() {
		return newArrayList(ao.find(AoProjectEmail.class, "ENABLED = ?", Boolean.TRUE));
	}

	@Override
	public AoProjectEmail createOrUpdate(int project, boolean enabled) {
		AoProjectEmail existing = get(project);
		if (existing != null) {
			existing.setEnabled(enabled);
			existing.save();
	        return existing;
		} else {
			return add(project, enabled);
		}
	}

}
