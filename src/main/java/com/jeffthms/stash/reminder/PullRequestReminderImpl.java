package com.jeffthms.stash.reminder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.avatar.AvatarService;
import com.atlassian.stash.mail.MailService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.SecurityService;

public class PullRequestReminderImpl implements PullRequestReminder, LifecycleAware {

	static final String KEY = PullRequestReminderImpl.class.getName() + ":instance";
    static final String JOB_NAME = PullRequestReminderImpl.class.getName() + ":job";
 
    private final Logger logger = Logger.getLogger(PullRequestReminderImpl.class);
    private final PluginScheduler pluginScheduler;  // provided by SAL
    private final PullRequestService pullRequestService;
    private final SecurityService securityService;
    private final MailService mailService;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final NavBuilder navBuilder;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final AvatarService avatarService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ProjectEmailService projectEmailService;
    private final UserSettingService userSettingService;

    private Date lastRun = null;        				// time when the last search returned
    
    public PullRequestReminderImpl(PluginScheduler pluginScheduler, PullRequestService pullRequestService, 
    		SecurityService securityService, MailService mailService, ApplicationPropertiesService applicationPropertiesService,
    		NavBuilder navBuilder, PluginSettingsFactory pluginSettingsFactory, SoyTemplateRenderer soyTemplateRenderer, 
    		AvatarService avatarService, ProjectEmailService projectEmailService, UserSettingService userSettingService) {
        this.pluginScheduler = pluginScheduler;
        this.pullRequestService = pullRequestService;
        this.securityService = securityService;
        this.mailService = mailService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.navBuilder = navBuilder;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.avatarService = avatarService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.projectEmailService = projectEmailService;
        this.userSettingService = userSettingService;
    }
    
	@Override
	public void onStart() {
		schedule();
	}

	@Override
	public void schedule() {
		final PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
		        
        pluginScheduler.scheduleJob(
                JOB_NAME,                   // unique name of the job
                PullRequestReminderTask.class,     // class of the job
                new HashMap<String,Object>() {{
                    put(KEY, PullRequestReminderImpl.this);
                    put("pullRequestService", pullRequestService);
                    put("securityService", securityService);
                    put("mailService", mailService);
                    put("applicationPropertiesService", applicationPropertiesService);
                    put("navBuilder", navBuilder);
                    put("pluginSettingsFactory", pluginSettingsFactory);
                    put("avatarService", avatarService);
                    put("soyTemplateRenderer", soyTemplateRenderer);
                    put("projectEmailService", projectEmailService);
                    put("userSettingService", userSettingService);
                }},                         					// data that needs to be passed to the job
                getStartTime(globalSettings),                 	// the time the job is to start
                getInterval(globalSettings));                  	// interval between repeats, in milliseconds
	}
	
	@Override
	public void reschedule() {		        
        pluginScheduler.unscheduleJob(JOB_NAME);
        schedule();
	}
	
	private Date getStartTime(PluginSettings pluginSettings) {
		String startTimeString = (String) pluginSettings.get(REMINDER_START_TIME);
		Date start;
		
		if (startTimeString == null) {
			startTimeString = DEFAULT_START_TIME;
		}
		
		try {
			start = new SimpleDateFormat("HH:mm").parse(startTimeString);
		} catch (ParseException e) {
			throw new IllegalStateException("Time is not in the HH:mm format", e);
		}
		
		logger.debug("Start time set to " + start);
		return start;
	}
	
	private long getInterval(PluginSettings pluginSettings) {
		String intervalString = (String) pluginSettings.get(REMINDER_INTERVAL);
		long interval;
		
		if (intervalString == null) {
			intervalString = DEFAULT_INTERVAL;
		}
		
		try {
			interval = Long.parseLong(intervalString);
		} catch (NumberFormatException e) {
			throw new IllegalStateException("Interval is not a number", e);
		}
		
		logger.debug("Interval set to " + interval);
		return interval;
	}
	
	public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }
	
	public Date getLastRun() {
		return lastRun;
	}
	
}
