package com.jeffthms.stash.reminder;

import java.util.List;

import com.atlassian.activeobjects.tx.Transactional;
import com.jeffthms.stash.reminder.model.AoProjectEmail;

@Transactional
public interface ProjectEmailService {
	AoProjectEmail add(int project, boolean enabled);
	AoProjectEmail add(int project);
	
	AoProjectEmail get(int project);
	
	AoProjectEmail createOrUpdate(int project, boolean enabled);
	 
    List<AoProjectEmail> all();
    List<AoProjectEmail> allEnabled();
}