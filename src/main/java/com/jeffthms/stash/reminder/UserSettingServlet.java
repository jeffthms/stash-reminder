package com.jeffthms.stash.reminder;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.google.common.collect.ImmutableMap;
import com.jeffthms.stash.reminder.model.AoUserSetting;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;

public class UserSettingServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(UserSettingServlet.class);
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final UserService userService;
    private final UserSettingService userSettingService;

    public UserSettingServlet(SoyTemplateRenderer soyTemplateRenderer, UserService userService,
    		UserSettingService userSettingService) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.userService = userService;
        this.userSettingService = userSettingService;
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	// Get userSlug from path
    	String pathInfo = req.getPathInfo();

    	String userSlug = pathInfo.substring(1); // Strip leading slash
    	StashUser user = userService.getUserBySlug(userSlug);

    	if (user == null) {
    	    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
    	    return;
    	}
    	    	
    	AoUserSetting userSetting = userSettingService.get(user.getId());
    	    	
    	if (userSetting == null) {
    		userSetting = userSettingService.add(user.getId());
    	}
    
    	render(resp, "user.setting", ImmutableMap.<String, Object>of("user", user, "sendEmail", userSetting.isSendEmail()));
    }
    
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// Get userSlug from path
    	String pathInfo = req.getPathInfo();

    	String userSlug = pathInfo.substring(1); // Strip leading slash
    	StashUser user = userService.getUserBySlug(userSlug);

    	if (user == null) {
    	    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
    	    return;
    	}
    	
    	String sendEmail = req.getParameter("sendEmail");
    	boolean sendEmailBoolean = false;
    	
    	if (StringUtils.isNotBlank(sendEmail)) {
            try {
                sendEmailBoolean = Boolean.parseBoolean(sendEmail);
            } catch (Exception e) {
                sendEmailBoolean = true;
            }
        } else {
            sendEmailBoolean = false;
        }
    	
    	AoUserSetting userSetting = userSettingService.setSendEmail(user.getId(), sendEmailBoolean);
    	
    	render(resp, "user.setting", ImmutableMap.<String, Object>of("user", user, "sendEmail", userSetting.isSendEmail()));
    }
    
    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.jeffthms.stash.reminder:reminder-soy-templates",
                    templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

}