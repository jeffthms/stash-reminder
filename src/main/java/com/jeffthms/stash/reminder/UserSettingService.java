package com.jeffthms.stash.reminder;

import java.util.List;

import com.jeffthms.stash.reminder.model.AoUserSetting;

public interface UserSettingService {
	AoUserSetting add(int user);
	
	AoUserSetting setSendEmail(int user, boolean sendEmail);
	
	AoUserSetting get(int user);
		 
    List<AoUserSetting> all();
}
